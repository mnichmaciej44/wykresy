#include "Wzor_func.h"


void wypiszWektor(vector<string> wektor){
  cout<<endl<<"Wypisz wektor: "<<endl;
  for(unsigned int i=0;i<wektor.size();i++){
    cout<<"\tElement "<<i<<": "<<wektor[i]<<endl;
  }
  cout<<"Koniec wypisywania\n";
}
void wypiszWektor(vector<double> wektor){
  cout<<endl<<"Wypisz wektor: "<<endl;
  for(unsigned int i=0;i<wektor.size();i++){
    cout<<"\tElement "<<i<<": "<<wektor[i]<<endl;
  }
  cout<<"Koniec wypisywania\n";
}
void wypiszWektor(vector<Element> wektor){
  cout<<endl<<"Wypisz wektor: "<<endl;
  for(unsigned int i=0;i<wektor.size();i++){
    if(wektor[i].typ == liczba)
        cout<<" "<<wektor[i].liczba;
    else 
        cout<<" "<<wektor[i].operacja;
  }
  cout<<"\nKoniec wypisywania\n";
}

bool Wzor_func::checkFunc(){
    bool check = true;
    if(func.length()<1) return false;
    for(unsigned int i=0;i<func.length();i++){
        int temp=0, temp2=0;
        for(int j=0;j<ilosc_doz_znakow;j++){
            if(dozwolone_znaki[j].length() == 1){
                if(func[i] == dozwolone_znaki[j][0]){
                    temp++;
                }
            }else{
                unsigned int temp3=0;
                for(unsigned int k=0;k<dozwolone_znaki[j].length();k++){
                    if(func[i+k] == dozwolone_znaki[j][k]) temp3++;
                }
                if(temp3 == dozwolone_znaki[j].length()) temp2++;
            }
            

        }
        if( temp != 1 && temp2!=1) check = false;
    }

    return check;
}

double Wzor_func::oblicz(double x){
    // cout<<"====1===";
    
    string temp = "";
    for(unsigned int i=0;i<func.length();i++){
        if(czy_znak(func[i])){
            if(temp!=""){
                Element elem;
                elem.typ = liczba;
                elem.liczba = stod(temp);
                temp = "";
                kolejka.push_back(elem);
            }
            Element elem;
            elem.typ = operacja;
            elem.operacja = func[i];
            kolejka.push_back(elem);
        }else if(func[i]=='x'){
            if(temp!=""){
                Element elem1;
                elem1.typ = liczba;
                elem1.liczba = stod(temp);
                temp = "";
                Element elem2;
                elem2.typ = operacja;
                elem2.operacja = "*";
                kolejka.push_back(elem1);
                kolejka.push_back(elem2);
            }
            Element elem;
            elem.typ = liczba;
            elem.liczba = x;
            kolejka.push_back(elem);
        }else if(func[i] == '(' || func[i] == ')'){
            if(temp!=""){
                Element elem1;
                elem1.typ = liczba;
                elem1.liczba = stod(temp);
                temp = "";
                Element elem2;
                elem2.typ = operacja;
                elem2.operacja = "*";
                kolejka.push_back(elem1);
                kolejka.push_back(elem2);
            }
            Element elem;
            elem.typ = nawias;
            elem.operacja = func[i];
            kolejka.push_back(elem);
        }else{
            temp+= func[i];
        }

    }
    if(temp!=""){
        Element elem;
        elem.typ = liczba;
        elem.liczba = stod(temp);
        kolejka.push_back(elem);
    }
    // cout<<"=== 1 ====";
    // wypiszWektor(kolejka);
    
    double wynik = 0;

    int poczNaw, zamknNaw;
    vector<Element> wewNawias;
    wewNawias = szukajWewNawK(&poczNaw,&zamknNaw);
    while(zamknNaw != -1 && wewNawias.size() > 1){
        // cout<<"\n\nWewnetrzynawias: ";
        // wypiszWektor(wewNawias);
        // cout<<endl;
        double wynNaw = licz(wewNawias,x);

        // cout<<"Wynik w nawiasie: "<<wynNaw;
        if( poczNaw-1<0 || kolejka[poczNaw-1].typ == operacja) replace(&kolejka,poczNaw,zamknNaw, to_string(wynNaw));
        else replace(&kolejka,poczNaw,zamknNaw, "*"+to_string(wynNaw));
        // cout<<"\nFunkcja po obliczeniu nawiasu: ";
        // wypiszWektor(kolejka);
        wewNawias = szukajWewNawK(&poczNaw,&zamknNaw);
    };
    wynik = licz(kolejka,x);
    kolejka.erase(kolejka.begin(),kolejka.end());
    return wynik;
}

string Wzor_func::szukajWewNaw(int* x, int *y){
    int zamknNaw = func.find(")");
    if(zamknNaw!=-1){
        int poczNaw = func.substr(0,zamknNaw).find_last_of("(");
        *x = poczNaw;
        *y = zamknNaw;
        return func.substr(poczNaw+1,zamknNaw-2-poczNaw);
    }else return "-1";
}
vector<Element> Wzor_func::szukajWewNawK(int *x, int* y){
    int zamknNaw = -1, poczNaw = -1;
    vector<Element> temp;
    for(unsigned int i=0;i<kolejka.size();i++){
        if(kolejka[i].typ == nawias){
            if(zamknNaw == -1 && kolejka[i].operacja == ")"){
                zamknNaw = i;
                break;
            }else if(kolejka[i].operacja == "("){
                poczNaw = i;
            }
        }
    }
    *x = poczNaw;
    *y = zamknNaw;
    if(zamknNaw == -1) zamknNaw = kolejka.size();
    // cout<<"=== 2 ===="<<endl;
    // cout<<"pocz: "<<*x<<" kon: "<<*y<<endl;
    for(int i=poczNaw+1;i<zamknNaw-1;i++){
        temp.push_back(kolejka[i]);
    }
    return temp;
}

bool Wzor_func::czy_znak(char z){
    bool czy_znak = false;
    for(int j=0;j<5;j++){
        if(z==dozwolone_dzialania[j]){
            czy_znak = true;
            break;
        }  
    }
    return czy_znak;
}

double Wzor_func::licz(vector<Element> kolejkaAktualna ,double x){
    
    double wynik = 0;

    // wypiszWektor(kolejkaAktualna);

    for(unsigned int i=0;i<kolejkaAktualna.size();i++){
        if(kolejkaAktualna[i].typ == operacja && kolejkaAktualna[i].operacja=="^"){
            double tempWynik = pow(kolejkaAktualna[i-1].liczba , kolejkaAktualna[i+1].liczba);
            tempWynik = sprZakres(tempWynik);
            kolejkaAktualna.erase(kolejkaAktualna.begin() +i, kolejkaAktualna.begin()+i+2);
            kolejkaAktualna[i-1].liczba = tempWynik;
        }
    }
    // wypiszWektor(kolejkaAktualna);
    
    for(unsigned int i=0;i<kolejkaAktualna.size();i++){
        if(kolejkaAktualna[i].typ == operacja && kolejkaAktualna[i].operacja=="*"){
            double tempWynik = kolejkaAktualna[i-1].liczba * kolejkaAktualna[i+1].liczba;
            tempWynik = sprZakres(tempWynik);
            kolejkaAktualna.erase(kolejkaAktualna.begin() +i, kolejkaAktualna.begin()+i+2);
            kolejkaAktualna[i-1].liczba = tempWynik;
        }
    }
    // wypiszWektor(kolejkaAktualna);
    
    for(unsigned int i=0;i<kolejkaAktualna.size();i++){
        if(kolejkaAktualna[i].typ == operacja && kolejkaAktualna[i].operacja=="/"){
            double tempWynik = kolejkaAktualna[i-1].liczba / kolejkaAktualna[i+1].liczba;
            tempWynik = sprZakres(tempWynik);
            kolejkaAktualna.erase(kolejkaAktualna.begin() +i, kolejkaAktualna.begin()+i+2);
            kolejkaAktualna[i-1].liczba = tempWynik;
        }
    }
    // wypiszWektor(kolejkaAktualna);
    
    for(unsigned int i=0;i<kolejkaAktualna.size();i++){
        if(kolejkaAktualna[i].typ == operacja && kolejkaAktualna[i].operacja=="-"){
            if(i!=0){
                double tempWynik = kolejkaAktualna[i-1].liczba - kolejkaAktualna[i+1].liczba;
                tempWynik = sprZakres(tempWynik);
                kolejkaAktualna.erase(kolejkaAktualna.begin() +i, kolejkaAktualna.begin()+i+2);
                kolejkaAktualna[i-1].liczba = tempWynik;
            }else{
                kolejkaAktualna[i+1].liczba *= -1;
                kolejkaAktualna.erase(kolejkaAktualna.begin());
                
            }
        }
    }
    // wypiszWektor(kolejkaAktualna);
    
    for(unsigned int i=0;i<kolejkaAktualna.size();i++){
        if(kolejkaAktualna[i].typ == operacja && kolejkaAktualna[i].operacja=="+"){
            double tempWynik = kolejkaAktualna[i-1].liczba + kolejkaAktualna[i+1].liczba;
            tempWynik = sprZakres(tempWynik);
            kolejkaAktualna.erase(kolejkaAktualna.begin() +i, kolejkaAktualna.begin()+i+2);
            kolejkaAktualna[i-1].liczba = tempWynik;
        }
    }
    // wypiszWektor(kolejkaAktualna);

    wynik = kolejkaAktualna[0].liczba;
    // cout<<endl<<"koniec"<<endl;
kolejkaAktualna.clear();
return wynik;
}
    
//   int resizeUp(string* tab, int n){
//     string* tmp = new string[0];

//     // string* tmp = new string[n];
//     for(int i=0;i<n;i++){
//       // tmp[i]= tab[i];
//     }
//     cout<<"\nprzed: "<<n;
    
//     // cout<<"\npo: "<<n+1;
//     // tab = new string[n+1];
//     // for(int i=0;i<n;i++){
//     //   tab[i] = tmp[i];
//     // }
//     // tab[n] = "";
//     // delete[] tmp;
//     cout<<"dziala";
//     return n+1;
//   }
void Wzor_func::replace(vector<Element>* vec,int pocz, int kon,string wyn){
    if(wyn[0] == '*'){
        Element elem;
        elem.typ = operacja;
        elem.operacja = "*";
        (*vec)[pocz] = elem;
        Element elem2;
        elem2.typ = liczba;
        elem2.liczba = stod(wyn.substr(1));
        (*vec)[pocz+1] = elem2;
        (*vec).erase((*vec).begin()+pocz+2, (*vec).begin()+kon+1);
    }else{
        Element elem;
        elem.typ = liczba;
        elem.liczba = stod(wyn);
        (*vec)[pocz] = elem;
        (*vec).erase((*vec).begin()+pocz+1, (*vec).begin()+kon+1);
        
    }
}


bool Wzor_func::create(string wzor){
    kolejka.clear();
    string tablica[] = {
      "x","0","1","2","3","4","5","6","7","8","9","(",")",".",",","^","*","-","+","/","sqrt","sin","cos","tg","ctg"
    };
    dozwolone_znaki = tablica;
    ilosc_doz_znakow  = sizeof(tablica)/sizeof(tablica[0]);
    

    func = wzor;
    if(checkFunc()) return 1;
    else return 0;
}

double Wzor_func::sprZakres(double liczba){
    if(liczba < 0){
        if(fabs(liczba) < 1.0e-6) liczba = -1.0e-6;
        else if(fabs(liczba) > 1.0e+6) liczba = -1.0e+6;
    }else{
        if(fabs(liczba) < 1.0e-6) liczba = 1.0e-6;
        else if(fabs(liczba) > 1.0e+6) liczba = 1.0e+6;
    }
    return liczba;    
}