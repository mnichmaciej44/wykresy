#include "Wykres.h"

 Wykres::Wykres(){
    resX = 800;
    resY = 600;
    X0 = resX/2;
    Y0 = resY/2;
    rightSquare = 400;
   

    font.loadFromFile("/usr/share/fonts/truetype/ttf-khmeros-core/KhmerOS.ttf");
    gui.setFont("/usr/share/fonts/truetype/ttf-khmeros-core/KhmerOS.ttf");
    bgColor = Color(195, 221, 255);
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(Color::Red);
    text.setString(func);
    
    start();
    // ukladKart();
}

void Wykres::ukladKart(){
    double tempX0;
    double tempY0;
    long int odlMinMax;
    long int wartoscJednostkiY;
    long int wartoscJednostkiX;
    string tempOdlY;
    string wartoscJednostkiYS;
    string tempOdlX;
    string wartoscJednostkiXS;
    std::vector<sf::Vertex> skalaX;
    std::vector<sf::Vertex> skalaY;
    std::vector<sf::Vertex> linieUkladuY;
    std::vector<sf::Vertex> linieUkladuX;
    std::vector<sf::Vertex> tloX,tloY;
    std::vector<sf::Vertex> strzalki;
    Color skalaColor = Color::Red;
    doRysowaniaLinie.clear();
    doRysowaniaSkalaX.clear();
    doRysowaniaSkalaY.clear();
    punkty.clear();



    iloscOznSkalX = ileOzn(poczatekPrzedzialu,koniecPrzedzialu);
    wspSkalX = (resX-2*margines)/(double)iloscOznSkalX;
    
    if(iloscOznSkalX>9){
        tempOdlX = to_string(iloscOznSkalX);
        wartoscJednostkiXS = tempOdlX.substr(0,1);
        for(unsigned int i=1;i<tempOdlX.length()-1;i++){
            wartoscJednostkiXS += "0";
        }
        wartoscJednostkiX = stol(wartoscJednostkiXS);
    }else{
        wartoscJednostkiX = 1;
    }
    odlOznSkalX = (resX-2*margines) / (iloscOznSkalX/wartoscJednostkiX);

    
    if(poczatekPrzedzialu<=0 && koniecPrzedzialu>=0){
        tempX0 = margines + odlOznSkalX*fabs(poczatekPrzedzialu)/wartoscJednostkiX;
    }else if(poczatekPrzedzialu > 0 && koniecPrzedzialu > 0){
        tempX0 = margines/2;
    }else{
        tempX0 = resX - margines/2;
    }
    X0 = tempX0;
    
    

    liczPunkty();
    getMinMax();
    //===============oznaczenia Y
    odlMinMax = ileOzn(minY,maxY);
    wspSkalY = (resY-2*margines)/(double)(odlMinMax);
    odlMinMax++;
    // cout<<"\nWspY: "<<wspSkalY<<"\tWspX: "<<wspSkalX;
    //OdlOznY
    if(odlMinMax>9){
        tempOdlY = to_string(odlMinMax);
        wartoscJednostkiYS = tempOdlY.substr(0,1);
        for(unsigned int i=1;i<tempOdlY.length()-1;i++){
            wartoscJednostkiYS += "0";
        }

        wartoscJednostkiY = stol(wartoscJednostkiYS);

    }else{
        wartoscJednostkiY = 1;
    }
    odlOznSkalY = (resY-2*margines) / (odlMinMax/wartoscJednostkiY);


    if(minY<=0 && maxY>=0){
        tempY0 = margines + odlOznSkalY*fabs(minY)/wartoscJednostkiY;
    }else if(minY > 0 && maxY > 0){
        tempY0 = margines/2;
    }else{
        tempY0 = resY - margines/2;
    }
    Y0 = resY - tempY0;


    // cout<<"Ustalono cechy skali X oraz Y.\n";
    cout<<"Skala X:\n";
    cout<<"\tRozdzielczość X: "<<resX<<"\n";
    cout<<"\tX0: "<<X0<<"\n";
    cout<<"\tOdległość oznaczeń skali X: "<<odlOznSkalX<<"\n";
    cout<<"\tIlość oznaczeń skali X: "<<iloscOznSkalX<<"\n";
    cout<<"\tWartość jednostki X: "<<wartoscJednostkiX<<"\n";
    cout<<"\tPoczątek przedziału: "<<poczatekPrzedzialu<<"\n";
    cout<<"\tKoniec przedziału: "<<koniecPrzedzialu<<"\n";

    cout<<"\nSkala Y:\n";
    cout<<"\tRozdzielczość Y: "<<resY<<"\n";
    cout<<"\tY0: "<<Y0<<"\n";
    cout<<"\tOdległość oznaczeń skali Y: "<<odlOznSkalY<<"\n";
    cout<<"\tWartość jednostki Y: "<<wartoscJednostkiY<<"\n";
    cout<<"\tMin: "<<minY<<"\n";
    cout<<"\tMax: "<<maxY<<"\n";
    cout<<"\tOdległość min do max: "<<odlMinMax<<"\n";


    
    for(int i=X0;i<=resX;i+=odlOznSkalX){
        // cout<<"\n\t i = "<<i;

        skalaX.push_back(sf::Vertex(sf::Vector2f(i,Y0-5),skalaColor));
        skalaX.push_back(sf::Vertex(sf::Vector2f(i,Y0+5),skalaColor));
        tloX.push_back(sf::Vertex(sf::Vector2f(i,0),linieTlaColor));
        tloX.push_back(sf::Vertex(sf::Vector2f(i,resY),linieTlaColor));
    }
    
    for(int i=X0;i>=0;i-=odlOznSkalX){
        skalaX.push_back(sf::Vertex(sf::Vector2f(i,Y0-5),skalaColor));
        skalaX.push_back(sf::Vertex(sf::Vector2f(i,Y0+5),skalaColor));
        tloX.push_back(sf::Vertex(sf::Vector2f(i,0),linieTlaColor));
        tloX.push_back(sf::Vertex(sf::Vector2f(i,resY),linieTlaColor));
    }
    
    // cout<<"\nodleglosc oznaczeń skali Y:"<<odlOznSkalY;
    // cin>>stop;
    for(int i=Y0;i<=resY;i+=odlOznSkalY){
        // cout<<"\n\t i = "<<i;
        skalaY.push_back(sf::Vertex(sf::Vector2f(X0-5,i),skalaColor));
        skalaY.push_back(sf::Vertex(sf::Vector2f(X0+5,i),skalaColor));
        tloY.push_back(sf::Vertex(sf::Vector2f(0,i),linieTlaColor));
        tloY.push_back(sf::Vertex(sf::Vector2f(resX,i),linieTlaColor));

    }
    for(int i=Y0;i>=0;i-=odlOznSkalY){
        skalaY.push_back(sf::Vertex(sf::Vector2f(X0-5,i),skalaColor));
        skalaY.push_back(sf::Vertex(sf::Vector2f(X0+5,i),skalaColor));
        tloY.push_back(sf::Vertex(sf::Vector2f(0,i),linieTlaColor));
        tloY.push_back(sf::Vertex(sf::Vector2f(resX,i),linieTlaColor));
    }
    vector<Vertex> strzalkaY1,strzalkaX1,strzalkaY2,strzalkaX2;
    strzalkaY1.push_back(Vertex(Vector2f(X0,0),skalaColor));
    strzalkaY1.push_back(Vertex(Vector2f(X0-5,5),skalaColor));
    strzalkaY2.push_back(Vertex(Vector2f(X0,0),skalaColor));
    strzalkaY2.push_back(Vertex(Vector2f(X0+5,5),skalaColor));

    strzalkaX1.push_back(Vertex(Vector2f(resX-5,Y0-5),skalaColor));
    strzalkaX1.push_back(Vertex(Vector2f(resX,Y0),skalaColor));
    strzalkaX2.push_back(Vertex(Vector2f(resX-5,Y0+5),skalaColor));
    strzalkaX2.push_back(Vertex(Vector2f(resX,Y0),skalaColor));
    
    // cout<<"Naniesiono osie na obraz.\n";


    //=============
    
    linieUkladuY.push_back(sf::Vertex(sf::Vector2f(X0, 0), skalaColor));
    linieUkladuY.push_back(sf::Vertex(sf::Vector2f(X0, resY), skalaColor));

    linieUkladuX.push_back(sf::Vertex(sf::Vector2f(0, Y0), skalaColor));
    linieUkladuX.push_back(sf::Vertex(sf::Vector2f(resX, Y0), skalaColor));
    //skalaX
    // for(int i=0;i<=iloscOznSkalX;i++){
    //   doRysowaniaSkalaX.push_back(Text(std::to_string((int)(poczatekPrzedzialu+1*i)),font,12));
    //   doRysowaniaSkalaX[i].setPosition(margines+odlOznSkalX*i-5,Y0+7);
    //   doRysowaniaSkalaX[i].setFillColor(Color::Red);
    // }
    for(int i=X0,count=0;i>=0;i-=odlOznSkalX,count-=wartoscJednostkiX){
        Text temp = Text(std::to_string(count),font,12);
        temp.setPosition(i-8,Y0+7);
        temp.setFillColor(skalaColor);
        doRysowaniaSkalaY.push_back(temp);
    }
    for(int i=X0,count=0;i<=resX;i+=odlOznSkalX,count+=wartoscJednostkiX){
        Text temp = Text(std::to_string(count),font,12);
        temp.setPosition(i-8,Y0+7);
        temp.setFillColor(skalaColor);
        doRysowaniaSkalaY.push_back(temp);
    }

    //skalaY
    for(int i=Y0,count=0;i<=resY;i+=odlOznSkalY,count-=wartoscJednostkiY){
        Text temp = Text(std::to_string(count),font,12);
        temp.setPosition(X0+7,i-8);
        temp.setFillColor(skalaColor);
        doRysowaniaSkalaY.push_back(temp);
    }
    for(int i=Y0,count=0;i>=0;i-=odlOznSkalY,count+=wartoscJednostkiY){
        Text temp = Text(std::to_string(count),font,12);
        temp.setPosition(X0+7,i-8);
        temp.setFillColor(skalaColor);
        doRysowaniaSkalaY.push_back(temp);
    }


    
    doRysowaniaLinie.push_back(tloX);
    doRysowaniaLinie.push_back(tloY);
    doRysowaniaLinie.push_back(linieUkladuX);
    doRysowaniaLinie.push_back(linieUkladuY);
    doRysowaniaLinie.push_back(strzalkaX1);
    doRysowaniaLinie.push_back(strzalkaX2);
    doRysowaniaLinie.push_back(strzalkaY1);
    doRysowaniaLinie.push_back(strzalkaY2);
    doRysowaniaLinie.push_back(skalaX);
    doRysowaniaLinie.push_back(skalaY);
    punktyNaWykres();


}


void Wykres::rysuj(){
    window.clear(bgColor);
    window.draw(text);
    window.draw(label);
    window.draw(eqLabel);
    window.draw(resLabel);

    for(unsigned int i=0;i<doRysowaniaLinie.size();i++){
        window.draw(&doRysowaniaLinie[i][0],doRysowaniaLinie[i].size(),sf::Lines);
    }
    for(unsigned int i=0;i<doRysowaniaSkalaX.size();i++){
        window.draw(doRysowaniaSkalaX[i]);
    }

    for(unsigned int i=0;i<doRysowaniaSkalaY.size();i++){
        window.draw(doRysowaniaSkalaY[i]);
    }
    // window.draw(&punkty[0],punkty.size(),sf::Lines);

    gui.draw();
    window.display();
}

void Wykres::getMinMax(){
    double maxTemp=0, minTemp=0;
    if(punkty.size()>0){
        maxTemp = punkty[0].y;
        minTemp = punkty[0].y;
    }
    for(unsigned int i=1;i<punkty.size();i++){
        if(punkty[i].y > maxTemp){
            maxTemp = punkty[i].y;
        }
        if(punkty[i].y<minTemp){
            minTemp = punkty[i].y;
        }
    }


    minY = minTemp;
    maxY = maxTemp;

    // cout<<"\nObliczono wartości najmniejsze i największe.\n";
}


void Wykres::liczPunkty(){
    for(double i = poczatekPrzedzialu;i<=koniecPrzedzialu;i+=epsilon){
        // cout<<"f(i):"<<i<<endl;
        // if(fabs(i)>1.0e-7 ){
            double wartosc = funkcja.oblicz(i);
            Punkt temp(i,wartosc);
            // cout<<"wartosc "<<i<<"="<<wartosc<<endl;
            punkty.push_back(temp);
        // }else{
        //     Punkt temp(i,1.0e-7);
        //     punkty.push_back(temp);
        // }
        // cout<<"czy x:"<<czyXwMianowniku();
        // if(i >= -0.09 && i <0.09 && czyXwMianowniku()){
        //     double j = 0.00001;
        //     Punkt temp(j,1.0e+7);
        //     punkty.push_back(temp);
        // }
    }
}

void Wykres::punktyNaWykres(){
    Vertex prev;
    Color lineColor = Color(0,0,0);
    if(punkty.size()>0) prev = Vertex(sf::Vector2f(X0 + punkty[0].x*wspSkalX, Y0 - punkty[0].y * wspSkalY),lineColor); 
    for(unsigned int i=1;i<punkty.size();i++){
        // cout<<"i: "<<i<<" y:"<<punkty[i].y<<endl;
        vector<sf::Vertex> temp;
        if(fabs(punkty[i].y)<1.0e-6){
            sf::Vertex act = Vertex(sf::Vector2f(X0 + punkty[i].x*wspSkalX, Y0 - 1.0e-6 *wspSkalY),lineColor);
            temp.push_back(prev);
            temp.push_back(act);
            prev = act;



        }else if(fabs(punkty[i].y)>1.0e+6){
                prev = Vertex(sf::Vector2f(X0 + punkty[i].x*wspSkalX, Y0 - 1.0e+6 *wspSkalY),lineColor);
        }else{
            sf::Vertex act = Vertex(sf::Vector2f(X0 + punkty[i].x*wspSkalX, Y0 - punkty[i].y *wspSkalY),lineColor);
            temp.push_back(prev);
            temp.push_back(act);
            prev = act;

        }

        doRysowaniaLinie.push_back(temp);
    }
}

int Wykres::ileOzn(int x,int y){
    return fabs(y-x);
}
double Wykres::ileOzn(double x,double y){
    double temp = fabs(y-x);
    if(temp < 1.e-15) temp = 1.e-15;
    if(temp > 1.e+15) temp = 1.e+15;
    return temp;
}

void prepareFunc(Wykres* w,tgui::EditBox::Ptr fBox,tgui::EditBox::Ptr pPrz,tgui::EditBox::Ptr kPrz,tgui::Button::Ptr countBtn){
    Wzor_func f;
    string pPrzS = pPrz->getText().toAnsiString();
    string kPrzS = kPrz->getText().toAnsiString();
    if( pPrzS.length()>0 && kPrzS.length()>0  && f.create(fBox->getText().toAnsiString())){
        w->funkcja = f;
        w->func = f.func;
        w->poczatekPrzedzialu = stod(pPrzS);
        w->koniecPrzedzialu = stod(kPrzS);
        w->ukladKart();
        countBtn->setEnabled(true);
    }
}
void licz(Wykres* w,tgui::EditBox::Ptr input){
    double arg = stod(input->getText().toAnsiString());
    w->resLabel.setString(to_string(w->funkcja.oblicz(arg)));
}




void Wykres::start(){
 

    window.create(VideoMode(resX+rightSquare, resY), "Wykresy!");
    gui.setTarget(window);
    guiCreate();
    while (window.isOpen()){
    events();     
    rysuj();
    }
}

void Wykres::events(){
    sf::Event event;
    while (window.pollEvent(event)){
        gui.handleEvent(event);
        switch (event.type){
        case sf::Event::Closed:
            window.close();
            break;
        case sf::Event::KeyPressed:
            if(event.key.code == Keyboard::Escape){
                window.close();
            }
            break;
        default:
            break;
        }
    }
}

bool Wykres::czyXwMianowniku(){
    regex wzorzec( "(.*)\\d/(\\(+(.*)x(.*)\\)+)(.*)" );
    if(regex_match(func,wzorzec)){
        return 1;
    }
    else return 0;
}

void Wykres::guiCreate(){
    Color fontColor = Color::Black;
    auto button = tgui::Button::create("Rysuj");
    button->setPosition(resX+100,"80%");
    button->setSize(200, 40);
    gui.add(button);

    auto editBoxWzor = tgui::EditBox::create();
    editBoxWzor->setSize(300,40);
    editBoxWzor->setPosition(resX+50, "10%");
    editBoxWzor->setDefaultText("Podaj wzor funkcji");
    editBoxWzor->setInputValidator(tgui::EditBox::Validator::Int);
    editBoxWzor->setInputValidator("[0-9\\(\\)x+*/^\\-x]*");
    gui.add(editBoxWzor);

    auto editBoxPocz = tgui::EditBox::create();
    editBoxPocz->setSize(140,40);
    editBoxPocz->setPosition(resX+50, "20%");
    editBoxPocz->setDefaultText("Poczatek przedzialu");
    editBoxPocz->setInputValidator(tgui::EditBox::Validator::Int);
    editBoxPocz->setInputValidator("[-]?[0-9]*");
    gui.add(editBoxPocz);

    auto editBoxKon = tgui::EditBox::create();
    editBoxKon->setSize(140,40);
    editBoxKon->setPosition(resX+210, "20%");
    editBoxKon->setDefaultText("Koniec przedzialu");
    editBoxKon->setInputValidator(tgui::EditBox::Validator::Int);
    editBoxKon->setInputValidator("[-]?[0-9]*");
    gui.add(editBoxKon);

    auto editBoxArg = tgui::EditBox::create();
    editBoxArg->setSize(100,40);
    editBoxArg->setPosition(resX+105, "40%");
    editBoxArg->setDefaultText("x");
    editBoxArg->setInputValidator(tgui::EditBox::Validator::Int);
    editBoxArg->setInputValidator("[-]?[0-9]*");
    gui.add(editBoxArg);
    
    
    label.setFont(font);
    label.setCharacterSize(26);
    label.setFillColor(fontColor);
    label.setString("f(x)=");
    label.setPosition(resX+50, (resY/100)*40);

    eqLabel.setFont(font);
    eqLabel.setCharacterSize(26);
    eqLabel.setFillColor(fontColor);
    eqLabel.setString("=");
    eqLabel.setPosition(resX+210, (resY/100)*40);

    resLabel.setFont(font);
    resLabel.setCharacterSize(26);
    resLabel.setFillColor(fontColor);
    resLabel.setPosition(resX+225, (resY/100)*40);

    auto countBtn = tgui::Button::create("Licz");
    countBtn->setPosition(resX+100,"50%");
    countBtn->setSize(200, 40);
    countBtn->setEnabled(false);
    gui.add(countBtn);

    // auto label = tgui::Label::create();
    // label->setText("f(x)=");
    // label->setPosition(resX+210, "30%");
    // label->setTextSize(18);
    // gui.add(label);

    button->connect("pressed",prepareFunc, this, editBoxWzor,editBoxPocz,editBoxKon,countBtn);
    countBtn->connect("pressed",licz,this,editBoxArg);

}
