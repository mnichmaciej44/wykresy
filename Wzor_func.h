#pragma once
#include <string>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <math.h>

using namespace std;

enum Typ{
  liczba,
  operacja,
  nawias
};
struct Element{
    Typ typ;
    double liczba;
    string operacja;
};


class Wzor_func {
  public:
    string func;
    string temp;
    string* dozwolone_znaki;
    char dozwolone_dzialania[5] = {'+','-','*','/','^'};
    int ilosc_doz_znakow;
    vector<Element> kolejka;


    Wzor_func(){}

    string getFunc(){
      return func;
    }

    bool checkFunc();

    double oblicz(double );

    string szukajWewNaw(int* , int *);
    vector<Element> szukajWewNawK(int* , int *);

    bool czy_znak(char );

    double licz(vector<Element> , double);
    double sprZakres(double);
     
    void replace(vector<Element>*,int, int, string);

    bool create(string);


};