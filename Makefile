all: main run clean

main: main.o Wykres.o Wzor_func.o
	g++ main.o Wykres.o Wzor_func.o -o sfml-app -lsfml-graphics -lsfml-window -lsfml-system -std=c++11 -ltgui

main.o: main.cpp Wykres.h Wzor_func.h
	g++ -c  -Wall -pedantic main.cpp

Wykres.o: Wykres.h Wykres.cpp Wzor_func.h
	g++ -c  -Wall -pedantic Wykres.cpp

Wzor_func.o: Wzor_func.h Wzor_func.cpp
	g++ -c  -Wall -pedantic Wzor_func.cpp

run:
	./sfml-app

clean:
	rm *.o