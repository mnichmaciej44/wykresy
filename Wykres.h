#pragma once
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>
#include <string>
#include <iostream>
#include <cstdlib>
#include <regex>
#include <vector>
#include "Wzor_func.h"

using namespace sf;
using namespace std;

class Punkt{
  public:
    double x,y;
    Punkt(double a, double b){
      x = a;
      y = b;
    }
    Punkt(){}
};

class Wykres{
  public:
    Wykres();

  private:
    Wzor_func funkcja;
    int resX,resY,rightSquare;
    int X0,Y0;
    int margines = 50;
    double poczatekPrzedzialu,koniecPrzedzialu;
    double minY,maxY;
    int iloscOznSkalX,iloscOznSkalY;
    double odlOznSkalX,odlOznSkalY;
    double wspSkalY,wspSkalX;
    double epsilon = 0.1;
    string func;
    Font font;
    Color bgColor,linieTlaColor = Color(174, 176, 255);
    Text text,label,eqLabel,resLabel;
    RenderWindow window;
    tgui::Gui gui;
    vector<vector<sf::Vertex>> doRysowaniaLinie;
    vector<Punkt> punkty;
    vector<sf::Text> doRysowaniaSkalaX;
    vector<sf::Text> doRysowaniaSkalaY;

    void ukladKart();
    void rysuj();
    void getMinMax();
    void liczPunkty();
    void punktyNaWykres();
    int ileOzn(int ,int );
    double ileOzn(double ,double );
    void start();
    void events();
    bool czyXwMianowniku();
    void guiCreate();
    friend void prepareFunc(Wykres*, tgui::EditBox::Ptr,tgui::EditBox::Ptr,tgui::EditBox::Ptr,tgui::Button::Ptr);
    friend void licz(Wykres* ,tgui::EditBox::Ptr);
};
